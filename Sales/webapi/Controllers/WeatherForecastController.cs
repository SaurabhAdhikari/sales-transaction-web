using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Sales.Application.DTOs.UserDTOs;
using Sales.Domain.Entities;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Sales.Infrastructure.DependencyInjection;
using Sales.Application.IServices;

namespace webapi.Controllers;

[ApiController]
[ApiExplorerSettings(IgnoreApi =false)]
[Route("CustomerController", Name = "CustomerController")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;
    private readonly IConfiguration _config;
    private readonly IUserService _userService;




    public WeatherForecastController(ILogger<WeatherForecastController> logger, IConfiguration config, IUserService userService)
    {
        _logger = logger;
        _config = config;
        _userService = userService;
    }


    [HttpGet("all-users")]
    public IActionResult GetAllUsers()
    {
        var data = _userService.GetAllUser();
        
        return Ok(data);
    }

    [HttpPost("add")]
    public IActionResult AddUser(InsertUserDTO newUser)
    {
        try
        {
            var data = _userService.AddUser(newUser);
            return Ok(data);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("{Id}")]
    public IActionResult GetById(Guid Id)
    {
        try
        {
            return Ok(_userService.GetUser(Id));
        }catch(Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }
}
