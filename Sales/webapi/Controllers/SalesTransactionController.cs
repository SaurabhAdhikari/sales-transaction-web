﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sales.Application.DTOs.SalesTransactionDTOs;
using Sales.Application.IServices;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesTransactionController : ControllerBase
    {
        private readonly ISalesTransactionService _salesTransactionService;
        public SalesTransactionController(ISalesTransactionService salesTransactionService)
        {
                _salesTransactionService = salesTransactionService;
        }

        [HttpPost("add")]
        public IActionResult AddSales(InputSalesTransactionDTO sales)
        {
            try
            {
                return Ok(_salesTransactionService.Add(sales));
            }catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("all-sales")]
        public IActionResult GetAllSales()
        {
            try
            {
                return Ok(_salesTransactionService.GetAll());

            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("update")]
        public IActionResult UpdateSale(UpdateSaleDTO sale)
        {
            try
            {
                return Ok(_salesTransactionService.Update(sale));

            }catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
