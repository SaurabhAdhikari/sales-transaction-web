﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sales.Application.DTOs.AuthDTOs;
using Sales.Application.IServices;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("login")]
        public IActionResult Login(LoginDTO login)
        {
            try
            {
                var islogin = _authService.Login(login);
                if (islogin)
                {
                    return Ok("Login Successfully");
                }
                return BadRequest("Invalid username or password");
            }catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }
    }
}
