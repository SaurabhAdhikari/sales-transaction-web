﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sales.Application.DTOs.ProductDTOs;
using Sales.Application.IServices;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [HttpPost("add")]
        public IActionResult AddProduct(AddProductDTO newProduct)
        {
            try
            {
                return Ok(_productService.Add(newProduct));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("all-products")]
        public IActionResult GetAllProducts()
        {
            try
            {
                return Ok(_productService.GetAll());
            }
            catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("update")]
        public IActionResult UpdateProduct(UpdateProductDTO product)
        {
            try
            {
                return Ok(_productService.Update(product));
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetProduct(Guid id)
        {
            var data = _productService.Get(id);
            return Ok(data);
        }
    }
}
