﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sales.Application.DTOs.InvoiceDTOs;
using Sales.Application.IServices;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly IInvoiceService _invoiceService;

        public InvoiceController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        [HttpPost("generate-invoice")]
        public IActionResult GenerateInvoice(AddInvoiceDTO invoice)
        {
            try
            {
                return Ok(_invoiceService.GenrateInvoice(invoice));

            }catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
