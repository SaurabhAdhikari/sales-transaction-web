import { Component, OnInit , Input} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthServiceService } from '../../services/auth-service.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthServiceService){}
  ngOnInit() {}
  // @Input() username : string="";
  // @Input() password : string="";

 @Input() loginObj:any={
    username:'',
    password:''
  }

  login(){
    console.log(this.loginObj.username);
    console.log(this.loginObj.password);
        this.authService.login(this.loginObj);
        // .subscribe(result=>{
        //   if(result.success){
        //     console.log(result);
        //     alert(result);
        //   }
        //   else{
        //     alert(result);
        //   }
        // })
    
  }
}

