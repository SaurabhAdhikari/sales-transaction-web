import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient, private snackBar:MatSnackBar) 
  {

  }

  GetProducts():Observable<any>{
    return this.http.get('https://localhost:7059/api/Product/all-products');
  }

  UpdateProduct(data:any):Observable<any>{
  console.log(data);
    this.http.put('https://localhost:7059/api/Product/update', data).subscribe(result=>{
      this.snackBar.open('Update successfully','',{duration:1000})
    }, (error: HttpErrorResponse)=>{
      this.snackBar.open('Unable to update changes!!','', {duration:1000},)
    });
    return this.http.get('https://localhost:7059/api/Product/all-products');
  }

  GetById(code:any):Observable<any>{
    var url ='https://localhost:7059/api/Product/'+code;
    console.log(url);
    return this.http.get(url);
  }

  AddProduct(data:any):void{
    console.log(data);
    this.http.post("https://localhost:7059/api/Product/add", data).subscribe(result=>{
      this.snackBar.open('Added successfully','',{duration:1000})
    }, (error: HttpErrorResponse)=>{
      this.snackBar.open('Unable to add product!!','', {duration:1000},)
    });
  }
}


// , (error:HttpErrorResponse)=>{
//   console.log(error);
//   if(error.status == 400){

//     this.snackBar.openFromComponent(LoginErrorComponent, {
//       duration: 1000
//     })
//   }
// });