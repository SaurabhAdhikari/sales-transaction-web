import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';
import { LoginComponent } from '../Components/login/login.component';
import { LoginErrorComponent } from '../Components/Errors/login-error/login-error.component';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http:HttpClient, private snackBar:MatSnackBar) 
  {

  }


  login(data:any):void{
    console.log(data);
     this.http.post('https://localhost:7059/api/Auth/login',data).subscribe(result=>{
      console.log(result);
      
    }, (error:HttpErrorResponse)=>{
      console.log(error);
      if(error.status == 400){

        this.snackBar.openFromComponent(LoginErrorComponent, {
          duration: 1000
        })
      }
    });
  }

  GetUser():Observable<any>{
    return this.http.get('https://localhost:7059/CustomerController/all-users');
  }

  GetProducts():Observable<any>{
    return this.http.get('https://localhost:7059/api/Product/all-products');
  }


}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

export interface UserModel{
  id:string,
  name:string,
  contact:string,
  email:string,
  username:string
}