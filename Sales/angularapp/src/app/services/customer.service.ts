import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http:HttpClient, private snackBar:MatSnackBar) 
  {

  }

  GetCustomer():Observable<any>{
    return this.http.get('https://localhost:7059/CustomerController/all-users');
  }

  AddCustomer(data:any){
   this.http.post("https://localhost:7059/CustomerController/add", data).subscribe(result=>{
    this.snackBar.open("customer added successfully",'',{duration:1000})
   }, (error:HttpErrorResponse)=>{
    this.snackBar.open("Unable to add customer",'',{duration:1000})
   }); 
  }

}
