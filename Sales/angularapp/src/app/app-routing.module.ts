import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes , RouterModule} from '@angular/router';
import { LandingComponent } from './Pages/landing/landing.component';
import { LoginComponent } from './Components/login/login.component';
import { ProductsComponent } from './Pages/products/products.component';

const routes: Routes=[
  {
    component: LandingComponent,
    path:'/landing'
  },
  {
    component: LoginComponent,
    path:''
  },
  {
    component: ProductsComponent,
    path:'/product'
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LandingComponent, LoginComponent, ProductsComponent]
