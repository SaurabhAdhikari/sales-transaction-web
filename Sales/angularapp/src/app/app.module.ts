import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavigationComponent } from './Components/navigation/navigation.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './Components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { LoginErrorComponent } from './Components/Errors/login-error/login-error.component';
import { Routes } from '@angular/router';
import { LandingComponent } from './Pages/landing/landing.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { ProductsComponent } from './Pages/products/products.component';
import { ModalComponent } from './Pages/modal/modal.component';
import { AddProductComponent } from './Pages/products/pro_modal/add-product/add-product.component';
import { AddCustomerComponent } from './Pages/landing/customer_modal/add-customer/add-customer.component';

const routes: Routes=[
  {
    component: LandingComponent,
    path:'/dashboard'
  },
  {
    component: LoginComponent,
    path:''
  }
]
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    LoginErrorComponent,
    LandingComponent,
    ProductsComponent,
    ModalComponent,
    AddProductComponent,
    AddCustomerComponent
  ],
  imports: [
    BrowserModule, 
    HttpClientModule, 
    AppRoutingModule,
    FormsModule, 
    BrowserAnimationsModule, 
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatDialogModule
    
  ],
  providers: [],
  bootstrap: [AppComponent, LandingComponent]
})
export class AppModule { }
