import { Component, OnInit } from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import { AuthServiceService } from '../../services/auth-service.service';
import { CustomerService } from '../../services/customer.service';
import { MatDialog } from '@angular/material/dialog';
import { AddCustomerComponent } from './customer_modal/add-customer/add-customer.component';




@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  constructor(private customerService: CustomerService,private dialog: MatDialog){}
  dataSource :any;
  ngOnInit(): void {
   this.dataSource = this.customerService.GetCustomer(); 
  }
  displayedColumns: string[] = ['position','id', 'name', 'contact', 'email', 'username', 'address', 'actions'];


  openAddCustomerModal():void{
    var _popUP = this.dialog.open(AddCustomerComponent,{
      width: '50%',
      height: '400px'
    });

    _popUP.afterClosed().subscribe((item:string)=>{
      this.dataSource = this.customerService.GetCustomer();
      
    })
  }
}
