import { CustomerService } from '../../../../services/customer.service';
import { Component, Inject, OnInit, inject } from '@angular/core';
import { FormBuilder, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';


@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent {

  editData: any;
  inputData:any;

  constructor(@Inject(MAT_DIALOG_DATA) public data:any, private ref: MatDialogRef<AddCustomerComponent>,
    private builder: FormBuilder,
    private customerService: CustomerService) 
  {

  }
  ngOnInit(): void {
    this.inputData = this.data;
    this.SetPopUpdata(this.inputData);
  }



  closeModal() {
    this.ref.close();
  }
  addUserForm = this.builder.group({
    name: this.builder.control(''),
    contact: this.builder.control(''),
    email: this.builder.control(''),
    username: this.builder.control(''),
    password: this.builder.control(''),
    address : this.builder.control('')

  });
  AddCustomer() {
    var adduser = {
      name: this.addUserForm.value.name,
      contact: this.addUserForm.value.contact,
      email: this.addUserForm.value.email,
      username: this.addUserForm.value.username,
      password: this.addUserForm.value.password,
      address: [
        this.addUserForm.value.address
      ]

    };
    this.customerService.AddCustomer(adduser);

  }

  SetPopUpdata(setdata: any) {
   // this.addUserForm.setValue({ name:setdata.name, description: setdata.description, rate: setdata.rate, stock: setdata.stock });
    // this.productService.GetById(setdata.id).subscribe(result => {
    //   this.editData = result.item,
    //     this.myform.setValue({ name: this.editData.name, description: this.editData.description, rate: this.editData.rate, stock: this.editData.stock })
    // });
  }
}
