import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../services/auth-service.service';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { ModalComponent } from '../modal/modal.component';
import { ProductService } from '../../services/product.service';
import { AddProductComponent } from './pro_modal/add-product/add-product.component';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  dataSource: any;
  displayedColumns: string[] = ['position', 'id', 'name', 'description', 'perUnit', 'stock', 'actions'];


  constructor(private productService:ProductService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.dataSource = this.productService.GetProducts();
  }

  openDialog(data:any): void {
    var _popUP = this.dialog.open(ModalComponent, {
      width: '50%',
      height: '400px',
      data:{
        id: data.id,
        name: data.name,
        description : data.description,
        rate: data.perUnit,
        stock:data.stock
      }
    });

    _popUP.afterClosed().subscribe((item:string)=>{
      this.dataSource = this.productService.GetProducts();
      
    })
  }


  openAddModal():void{
    var _popUP = this.dialog.open(AddProductComponent,{
      width: '50%',
      height: '400px'
    });

    _popUP.afterClosed().subscribe((item:string)=>{
      this.dataSource = this.productService.GetProducts();
      
    })
  }
}
