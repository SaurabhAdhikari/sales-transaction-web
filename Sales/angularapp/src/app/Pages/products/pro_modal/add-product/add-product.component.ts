import { Component, Inject, OnInit, inject } from '@angular/core';
import { FormBuilder, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ProductService } from '../../../../services/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent {
  editData: any;
  inputData:any;

  constructor(@Inject(MAT_DIALOG_DATA) public data:any, private ref: MatDialogRef<AddProductComponent>,
    private builder: FormBuilder,
    private productService: ProductService) 
  {

  }
  ngOnInit(): void {

  }



  closeAddModal() {
    this.ref.close();
  }
  addProductFrom = this.builder.group({
    name: this.builder.control(''),
    description: this.builder.control(''),
    rate: this.builder.control(''),
    stock: this.builder.control('')

  });
  AddProduct() {
    console.log(this.addProductFrom.value)
    var addData = {
    
      name: this.addProductFrom.value.name,
      description: this.addProductFrom.value.description,
      perUnit: this.addProductFrom.value.rate,
      stock: this.addProductFrom.value.stock

    };
    this.productService.AddProduct(addData);

  }

  SetPopUpdata(setdata: any) {
    this.addProductFrom.setValue({ name:setdata.name, description: setdata.description, rate: setdata.rate, stock: setdata.stock });
  }
}
