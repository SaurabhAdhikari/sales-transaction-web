import { Component, Inject, OnInit, inject } from '@angular/core';
import { FormBuilder, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ProductService } from '../../services/product.service';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent implements OnInit{
  editData: any;
  inputData:any;

  constructor(@Inject(MAT_DIALOG_DATA) public data:any, private ref: MatDialogRef<ModalComponent>,
    private builder: FormBuilder,
    private productService: ProductService) 
  {

  }
  ngOnInit(): void {
    this.inputData = this.data;
    this.SetPopUpdata(this.inputData);
  }



  closeModal() {
    this.ref.close();
  }
  myform = this.builder.group({
    name: this.builder.control(''),
    description: this.builder.control(''),
    rate: this.builder.control(''),
    stock: this.builder.control('')

  });
  UpdateCustomer() {
    var updateData = {
      id: this.data.id,
      name: this.myform.value.name,
      description: this.myform.value.description,
      perUnit: this.myform.value.rate,
      stock: this.myform.value.stock

    };
    this.productService.UpdateProduct(updateData);

  }

  SetPopUpdata(setdata: any) {
    this.myform.setValue({ name:setdata.name, description: setdata.description, rate: setdata.rate, stock: setdata.stock });
    // this.productService.GetById(setdata.id).subscribe(result => {
    //   this.editData = result.item,
    //     this.myform.setValue({ name: this.editData.name, description: this.editData.description, rate: this.editData.rate, stock: this.editData.stock })
    // });
  }
}
