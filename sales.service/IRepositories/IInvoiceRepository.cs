﻿using Sales.Application.DTOs.InvoiceDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IRepositories
{
    public interface IInvoiceRepository
    {
        void GenerateInvoice(InsertInvoiceDTO newInvoice);
    }
}
