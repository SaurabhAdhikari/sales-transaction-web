﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IRepositories
{
    public interface IBaseRepository
    {

        DataTable ExecuteQuery(string procName, Dictionary<string, string> data );
    }
}
