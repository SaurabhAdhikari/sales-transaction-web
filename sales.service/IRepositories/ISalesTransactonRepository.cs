﻿using Sales.Application.DTOs.SalesTransactionDTOs;


namespace Sales.Application.IRepositories
{
    public interface ISalesTransactonRepository
    {
        ViewSalesTransactionDTO AddSales(AddSalesTransactionDTO newSale);
        List<ViewSalesTransactionDTO> GetAllSales();

        ViewSalesTransactionDTO UpdateSales(UpdateSalesTransactionDTO sale);
        ViewSalesTransactionDTO GetById (Guid id);
    }
}
