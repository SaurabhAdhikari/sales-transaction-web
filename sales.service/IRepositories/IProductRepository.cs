﻿using Sales.Application.DTOs.ProductDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IRepositories
{
    public interface IProductRepository
    {
        ViewProductDTO AddProduct(AddProductDTO newProduct);
        List<ViewProductDTO> AllProduct();
        ViewProductDTO UpdateProduct(UpdateProductDTO product);
        ViewProductDTO GetById(Guid Id);
    }
}
