﻿using Sales.Application.DTOs.AuthDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IRepositories
{
    public interface IAuthRepository
    {
        bool IsAuthenticated(LoginDTO login);
    }
}
