﻿using Sales.Application.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IRepositories
{
    public interface IUserRepository
    {
        List<ViewUserDTO> GetAllUsers();
        ViewUserDTO GetById(Guid id);
        ViewUserDTO AddUser(InsertUserDTO newUser);
        bool DeleteUser(Guid Id);
    }
}
