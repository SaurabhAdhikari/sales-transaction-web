﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.DTOs.InvoiceDTOs
{
    // user input
    public class AddInvoiceDTO
    {
        public Guid CustomerId { get; set; }
        public List<Guid> SaleIds { get; set; }
        public bool GenerateInvoice { get; set; }
    }

    // repo input

    public class InsertInvoiceDTO : AddInvoiceDTO
    {
        public decimal Total { get; set; }
        public int DiscountPercent => Total >= 500 ? 10 : 5;
        public decimal DiscountAmount => DiscountPercent == 5 ? Total * 0.005M : Total * 0.01M; 
    }
}
