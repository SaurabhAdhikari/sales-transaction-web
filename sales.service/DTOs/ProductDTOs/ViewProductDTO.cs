﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.DTOs.ProductDTOs
{
    public class ViewProductDTO : AddProductDTO
    {
        public Guid Id { get; set; }
    }
}
