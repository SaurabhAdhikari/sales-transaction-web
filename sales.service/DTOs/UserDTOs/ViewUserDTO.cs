﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.DTOs.UserDTOs
{
    public class ViewUserDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public List<string> Address { get; set; }
    }
}
