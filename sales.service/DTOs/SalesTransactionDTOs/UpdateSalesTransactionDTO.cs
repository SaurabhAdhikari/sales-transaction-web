﻿
namespace Sales.Application.DTOs.SalesTransactionDTOs
{
    public class UpdateSalesTransactionDTO : AddSalesTransactionDTO
    {
        public Guid Id { get; set; }
    }

    public class UpdateSaleDTO : InputSalesTransactionDTO 
    {
        public Guid Id { get; set; }
    }
}
