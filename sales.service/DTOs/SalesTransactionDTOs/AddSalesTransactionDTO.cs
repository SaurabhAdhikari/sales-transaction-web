﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.DTOs.SalesTransactionDTOs
{
    // this dto is for insert into database
    public class AddSalesTransactionDTO
    {
        public Guid ProductId { get; set; }
        public decimal Rate { get; set; }
        public int Quantity { get; set; }
    }
    // this dto is for user input
    // rate will fetch from database. user are not meant to provide product rate
    public class InputSalesTransactionDTO
    {
        public Guid ProductId { get; set;}
        public int Quantity { get; set; }
    }
}
