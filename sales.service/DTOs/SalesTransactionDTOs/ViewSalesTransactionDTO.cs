﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.DTOs.SalesTransactionDTOs
{
    public class ViewSalesTransactionDTO : AddSalesTransactionDTO
    {
        public Guid Id { get; set; }
        public decimal Total { get; set; }
        public string CreatedOn { get; set; }
        public string ProductName { get; set; }
    }
}
