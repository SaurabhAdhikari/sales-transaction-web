﻿using Sales.Application.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IServices
{
    public interface IUserService
    {
        //List<ViewUserDTO> GetAllUser();
        List<ViewUserDTO> GetAllUser();

        ViewUserDTO GetUser(Guid id);
        ViewUserDTO AddUser(InsertUserDTO newUser);
        string DeleteUser (Guid id);
    }
}
