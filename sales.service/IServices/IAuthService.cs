﻿using Sales.Application.DTOs.AuthDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IServices
{
    public interface IAuthService
    {
        bool Login(LoginDTO login);
    }
}
