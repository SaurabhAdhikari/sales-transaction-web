﻿using Sales.Application.DTOs.ProductDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IServices
{
    public interface IProductService
    {
        ViewProductDTO Add(AddProductDTO newProduct);
        List<ViewProductDTO> GetAll();
        ViewProductDTO Update(UpdateProductDTO product);
        ViewProductDTO Get(Guid Id);
    }
}
