﻿using Sales.Application.DTOs.SalesTransactionDTOs;
using Sales.Application.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IServices
{
    public interface ISalesTransactionService
    {
        ViewSalesTransactionDTO Add(InputSalesTransactionDTO sales);
        List<ViewSalesTransactionDTO> GetAll();
        ViewSalesTransactionDTO Update(UpdateSaleDTO sale);
    }
}
