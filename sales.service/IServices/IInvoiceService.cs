﻿using Sales.Application.DTOs.InvoiceDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Application.IServices
{
    public interface IInvoiceService
    {
        string GenrateInvoice(AddInvoiceDTO invoice);
    }
}
