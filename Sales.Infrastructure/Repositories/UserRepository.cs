﻿using Sales.Application.DTOs.UserDTOs;
using Sales.Application.IRepositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IBaseRepository _baseRepository;
        public UserRepository(IBaseRepository baseRepository)
        {
                _baseRepository = baseRepository;   
        }
        public ViewUserDTO AddUser(InsertUserDTO newUser)
        {
            var id = Guid.NewGuid();
            Dictionary<string, string> query = new Dictionary<string, string>() 
            {

                { "@action","ADD"},
                { "@id", $"{id}"},
                { "@name",newUser.Name},
                { "@contact", newUser.Contact},
                { "@username", newUser.Username},
                { "@password", newUser.Password },
                { "@email", newUser.Email }

            };

            var data = _baseRepository.ExecuteQuery("CustomerManagement", query);
            var addresses = new List<string>();
            foreach (var address in newUser.Address)
            {
                Dictionary<string, string> addressQuery = new Dictionary<string, string>() 
                {
                    { "@action", "ADD_ADDRESS"},
                    { "@Id",$"{id}"},
                    { "@addressId", Guid.NewGuid().ToString()},
                    { "@address",address}
                };
                _baseRepository.ExecuteQuery("CustomerManagement", addressQuery);
                addresses.Add(address);
            }
            return GetById(id);
        }

        public bool DeleteUser(Guid Id)
        {
            throw new NotImplementedException();
        }

        public List<ViewUserDTO> GetAllUsers()
        {
            List<ViewUserDTO> users = new List<ViewUserDTO>();
            Dictionary<string, string> query = new Dictionary<string, string> { {"@action", "GET" }};
            var data = _baseRepository.ExecuteQuery("CustomerManagement", query);

           
            foreach (DataRow user in data.Rows)
            {

                    users.Add(new ViewUserDTO
                    {
                        Name = user["Name"].ToString(),
                        Id = Guid.Parse(user["Id"].ToString()),
                        Contact = user["ContactNo"].ToString(),
                        Username = user["Username"].ToString(),
                        Email = user["email"].ToString()
                        
                    });
            }
           
            for(int i =0; i<users.Count; i++)
            {
                Dictionary<string, string> addressQuery = new Dictionary<string, string> {
                    { "@action", "GET_ADDRESS" },
                    {"@Id", users[i].Id.ToString() }
                };
                var address = _baseRepository.ExecuteQuery("CustomerManagement", addressQuery);
                var userAddresses = new List<string>();
                foreach (DataRow row in address.Rows)
                {
                    userAddresses.Add(row["Address"].ToString());
                }
                users[i].Address = userAddresses;
            }
          return users;
        }

        public ViewUserDTO GetById(Guid id)
        {
            Dictionary<string, string> query = new Dictionary<string, string>()
            {
                { "@action","GET_BY_ID"},
                { "@id", id.ToString()}
            };
           var data =  _baseRepository.ExecuteQuery("CustomerManagement", query);
            var addresses = new List<string>();
            var userAddresses = _baseRepository.ExecuteQuery("CustomerManagement", new Dictionary<string, string>()
            {
                {"@action","GET_ADDRESS" },
                { "@id", id.ToString()}
            });
            foreach(DataRow row in userAddresses.Rows) 
            {
                addresses.Add(row["address"].ToString());
            }
            return new ViewUserDTO
            {
                Id = Guid.Parse(data.Rows[0]["Id"].ToString()),
                Name = data.Rows[0]["Name"].ToString(),
                Contact = data.Rows[0]["ContactNo"].ToString(),
                Username = data.Rows[0]["username"].ToString(),
                Email = data.Rows[0]["email"].ToString(),
                Address = addresses
            };
        }
    }
}
