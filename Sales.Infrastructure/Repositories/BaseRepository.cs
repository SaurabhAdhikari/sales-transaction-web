﻿using Microsoft.Extensions.Configuration;
using Sales.Application.IRepositories;
using System.Data;
using System.Data.SqlClient;

namespace Sales.Infrastructure.Repositories
{
    public class BaseRepository : IBaseRepository
    {
        private readonly IConfiguration _configuration;
        private SqlConnection _connection;
        public BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            Initiate();

        }
        private void Initiate()
        {
            _connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection"));
        }
        private void EstablishConnection()
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

        }
        private void CloseConnection()
        {
            if (_connection.State != ConnectionState.Closed)
            {
                _connection.Close();
            }
        }

        public DataTable ExecuteQuery(string procName, Dictionary<string, string> data)
        {
            try
            {
                EstablishConnection();
                SqlCommand command = new SqlCommand(procName, _connection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                DataTable dataTable = new DataTable();

                foreach (var item in data)
                {
                    command.Parameters.AddWithValue(item.Key, item.Value);
                }
                command.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand = command;
                sqlDataAdapter.Fill(dataTable);
                CloseConnection();
                return dataTable;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }


    }
}
