﻿using Sales.Application.DTOs.SalesTransactionDTOs;
using Sales.Application.IRepositories;
using System.Data;

namespace Sales.Infrastructure.Repositories
{
    public class SalesTransactionRepository : ISalesTransactonRepository
    {
        private readonly IBaseRepository _baseRepository;
        private const string SalesManagement = "SalesManagement";
        public SalesTransactionRepository(IBaseRepository baseRepository)
        {
                _baseRepository = baseRepository;
        }

        public ViewSalesTransactionDTO AddSales(AddSalesTransactionDTO newSale)
        {
            var query = new Dictionary<string, string>()
            {
                { "@action","ADD"},
                { "@id", $"{ Guid.NewGuid()} "},
                { "@rate",newSale.Rate.ToString()},
                { "@quantity", newSale.Quantity.ToString()},
                { "@productId", newSale.ProductId.ToString()}
            };

            var data = _baseRepository.ExecuteQuery(SalesManagement, query);
            return new ViewSalesTransactionDTO
            {
                Id = Guid.Parse(data.Rows[0]["Id"].ToString()),
                ProductId = Guid.Parse(data.Rows[0]["productId"].ToString()),
                ProductName = data.Rows[0]["name"].ToString(),
                Rate = decimal.Parse( data.Rows[0]["rate"].ToString()),
                Quantity = int.Parse(data.Rows[0]["quantity"].ToString()),
                Total = decimal.Parse(data.Rows[0]["total"].ToString()),
                CreatedOn = data.Rows[0]["createdOn"].ToString()
            };
        }

        public List<ViewSalesTransactionDTO> GetAllSales()
        {
            var data = _baseRepository.ExecuteQuery(SalesManagement, new Dictionary<string, string>()
            {
                {"@action","GET"}
            });
            var sales = new List<ViewSalesTransactionDTO>();
            foreach (DataRow row in data.Rows)
            {
                sales.Add(new ViewSalesTransactionDTO
                {
                    Id = Guid.Parse(row["Id"].ToString()),
                    ProductId = Guid.Parse(row["productId"].ToString()),
                    ProductName = row["name"].ToString(),
                    Quantity = int.Parse(row["quantity"].ToString()),
                    Rate = decimal.Parse(row["rate"].ToString()),
                    Total = decimal.Parse(row["total"].ToString())
                });

            }
            return sales;
        }

        public ViewSalesTransactionDTO UpdateSales(UpdateSalesTransactionDTO sale)
        {
            var query = new Dictionary<string, string>()
            {
                { "@action","UPDATE"},
                { "@id", $"{ sale.Id}"},
                { "@rate",sale.Rate.ToString()},
                { "@quantity", sale.Quantity.ToString()},
                { "@productId", sale.ProductId.ToString()}
            };

            var data = _baseRepository.ExecuteQuery(SalesManagement, query);
            return new ViewSalesTransactionDTO
            {
                Id = Guid.Parse(data.Rows[0]["Id"].ToString()),
                ProductId = Guid.Parse(data.Rows[0]["productId"].ToString()),
                ProductName = data.Rows[0]["name"].ToString(),
                Rate = decimal.Parse(data.Rows[0]["rate"].ToString()),
                Quantity = int.Parse(data.Rows[0]["quantity"].ToString()),
                Total = decimal.Parse(data.Rows[0]["total"].ToString()),
                CreatedOn = data.Rows[0]["createdOn"].ToString()
            };
        }

        public ViewSalesTransactionDTO GetById(Guid id)
        {
            var data = _baseRepository.ExecuteQuery(SalesManagement, new Dictionary<string, string>()
            {
                { "@action", "GET_BY_ID"},
                { "@id",  id.ToString() }
            });
            return new ViewSalesTransactionDTO
            {
                Id = Guid.Parse(data.Rows[0]["Id"].ToString()),
                ProductId = Guid.Parse(data.Rows[0]["productId"].ToString()),
                ProductName = data.Rows[0]["name"].ToString(),
                Rate = decimal.Parse(data.Rows[0]["rate"].ToString()),
                Quantity = int.Parse(data.Rows[0]["quantity"].ToString()),
                Total = decimal.Parse(data.Rows[0]["total"].ToString()),
                CreatedOn = data.Rows[0]["createdOn"].ToString()
            };

        }
    }
}
