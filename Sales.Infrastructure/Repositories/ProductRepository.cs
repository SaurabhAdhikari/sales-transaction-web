﻿using Sales.Application.DTOs.ProductDTOs;
using Sales.Application.DTOs.UserDTOs;
using Sales.Application.IRepositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IBaseRepository _baseRepository;
        private const string ProductManagement = "ProductManagement";
        public ProductRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }
        public ViewProductDTO AddProduct(AddProductDTO newProduct)
        {
            Dictionary<string, string> query = new Dictionary<string, string>()
            {
                { "@action","ADD"},
                { "@id", $"{Guid.NewGuid()}"},
                { "@name", newProduct.Name},
                { "@desc",newProduct.Description},
                { "@perUnit", newProduct.PerUnit.ToString()},
                { "@stock", newProduct.Stock.ToString()}

            };

            var data = _baseRepository.ExecuteQuery(ProductManagement, query);
            return new ViewProductDTO
            {
                Id = Guid.Parse(data.Rows[0]["Id"].ToString()),
                Name = data.Rows[0]["Name"].ToString(),
                Description = data.Rows[0]["description"].ToString(),
                PerUnit =decimal.Parse( data.Rows[0]["rate"].ToString()),
                Stock = int.Parse( data.Rows[0]["stock"].ToString())

            };
        }

        public List<ViewProductDTO> AllProduct()
        {
            var products = new List<ViewProductDTO>();
            Dictionary<string, string> query = new Dictionary<string, string>()
            {
                { "@action","GET"}
            };
            var data = _baseRepository.ExecuteQuery(ProductManagement, query);
            
            foreach(DataRow row in data.Rows)
            {
                products.Add(new ViewProductDTO
                {
                    Id = Guid.Parse(row["id"].ToString()),
                    Name = row["name"].ToString(),
                    Description = row["Description"].ToString(),
                    Stock = int.Parse(row["stock"].ToString()),
                    PerUnit = decimal.Parse(row["rate"].ToString())
                });
            }
            return products;
        }

        public ViewProductDTO UpdateProduct(UpdateProductDTO product)
        {
            Dictionary<string, string> query = new Dictionary<string, string>()
            {
                { "@action","UPDATE"},
                { "@id", $"{product.Id}"},
                { "@name", product.Name},
                { "@desc",product.Description},
                { "@perUnit", product.PerUnit.ToString()},
                { "@stock", product.Stock.ToString()} 
            };

                var data = _baseRepository.ExecuteQuery(ProductManagement, query);
            return new ViewProductDTO
            {
                Id = Guid.Parse(data.Rows[0]["id"].ToString()),
                Description = data.Rows[0]["description"].ToString(),
                Name = data.Rows[0]["name"].ToString(),
                Stock =int.Parse( data.Rows[0]["stock"].ToString()),
                PerUnit = decimal.Parse(data.Rows[0]["rate"].ToString())
            };
        }

        public ViewProductDTO GetById(Guid Id)
        {
            var data = _baseRepository.ExecuteQuery(ProductManagement, new Dictionary<string, string>()
            {
                { "@action", "GET_BY_ID"},
                { "@id", $"{Id}"}
            });
            return new ViewProductDTO
            {
                Id = Guid.Parse(data.Rows[0]["id"].ToString()),
                Description = data.Rows[0]["description"].ToString(),
                Name = data.Rows[0]["name"].ToString(),
                Stock = int.Parse(data.Rows[0]["stock"].ToString()),
                PerUnit = decimal.Parse(data.Rows[0]["rate"].ToString())
            };
        }
    }
}
