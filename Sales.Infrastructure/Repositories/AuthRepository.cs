﻿using Sales.Application.DTOs.AuthDTOs;
using Sales.Application.IRepositories;
using Sales.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infrastructure.Repositories
{
    public class AuthRepository : IAuthRepository
    {
        private readonly IBaseRepository _baseRepository;
        public AuthRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }
        public bool IsAuthenticated(LoginDTO login)
        {
            Dictionary<string, string> query = new Dictionary<string, string>() {
                { "@action", "LOGIN" },
                { "username", login.Username },
                { "password", login.Password },
            };

            var data = _baseRepository.ExecuteQuery("LoginManagement", query);
            return Utils.Utils.VerifyHash(login.Password, data.Rows[0][4].ToString());
        }
    }
}
