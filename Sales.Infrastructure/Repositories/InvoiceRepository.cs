﻿using Sales.Application.DTOs.InvoiceDTOs;
using Sales.Application.IRepositories;

namespace Sales.Infrastructure.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly IBaseRepository _baseRepository;
        private const string InvoiceManagement = "InvoiceManagement";
        public InvoiceRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }
        public void GenerateInvoice(InsertInvoiceDTO newInvoice)
        {
            var id = Guid.NewGuid().ToString();
            var invoiceQuery = new Dictionary<string, string>() 
            {
                { "@action","GENERATE"},
                { "@id", id},
                { "@customerId", newInvoice.CustomerId.ToString()},
                { "@total", newInvoice.Total.ToString()},
                { "@discountPercent", newInvoice.DiscountPercent.ToString()},
                { "@discountAmount", newInvoice.DiscountAmount.ToString()}
            };
            var data = _baseRepository.ExecuteQuery(InvoiceManagement, invoiceQuery);
            if (newInvoice.GenerateInvoice)
            {
                foreach (var saleId in newInvoice.SaleIds)
                {
                    var saleQuery = new Dictionary<string, string>()
                    {
                        { "@action","SALE_INVOICE"},
                        { "@id", id},
                        { "@saleId", saleId.ToString()}
                    };
                    var saleInvoiceResponse = _baseRepository.ExecuteQuery(InvoiceManagement, saleQuery);
                }
            }
        }
    }
}
