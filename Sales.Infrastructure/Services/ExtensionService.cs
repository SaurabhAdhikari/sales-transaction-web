﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infrastructure.Services
{
    public class ExtensionService
    {
        private readonly IConfiguration _configuration;
        public ExtensionService(IConfiguration configuration)
        {
                _configuration = configuration;
        }

        public SqlConnection ConnectionOpen()
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection"));
            conn.Open();
            return conn;
        }
        public void ConnectionClose( SqlConnection connection)
        {
            connection.Close();
        }
    }
}
