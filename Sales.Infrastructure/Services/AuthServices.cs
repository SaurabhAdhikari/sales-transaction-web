﻿using Sales.Application.DTOs.AuthDTOs;
using Sales.Application.IRepositories;
using Sales.Application.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infrastructure.Services
{
    public class AuthServices : IAuthService
    {
        private readonly IAuthRepository _authRepository;
        public AuthServices(IAuthRepository authRepository)
        {
                _authRepository = authRepository;
        }
        public bool Login(LoginDTO login)
        {
            return _authRepository.IsAuthenticated(login);
        }
    }
}
