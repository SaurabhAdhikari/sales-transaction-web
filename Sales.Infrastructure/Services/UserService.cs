﻿using Sales.Application.DTOs.UserDTOs;
using Sales.Application.IServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Sales.Infrastructure.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Sales.Application.IRepositories;
using Sales.Infrastructure;

namespace Sales.Infrastructure.Services
{
    public class UserService : ExtensionService, IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IConfiguration configuration, IUserRepository userRepository) : base(configuration)
        {
            _userRepository = userRepository;
        }

        public ViewUserDTO AddUser(InsertUserDTO newUser)
        {
            newUser.Password = Utils.Utils.HashSecret(newUser.Password);
            return _userRepository.AddUser(newUser);
        }

        public string DeleteUser(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<ViewUserDTO> GetAllUser()
        {
            var data = _userRepository.GetAllUsers();
            return data;
        }

        public ViewUserDTO GetUser(Guid id)
        {
            return _userRepository.GetById(id);
        }
    }
}
