﻿using Sales.Application.DTOs.SalesTransactionDTOs;
using Sales.Application.IRepositories;
using Sales.Application.IServices;

namespace Sales.Infrastructure.Services
{
    public class SalesTransactionService : ISalesTransactionService
    {
        private readonly ISalesTransactonRepository _salesTransactonRepository;
        private readonly IProductRepository _productRepository;
        public SalesTransactionService(ISalesTransactonRepository salesTransactonRepository, IProductRepository productRepository)
        {
            _salesTransactonRepository = salesTransactonRepository;
            _productRepository = productRepository;

        }
        public ViewSalesTransactionDTO Add(InputSalesTransactionDTO sales)
        {
            var product = _productRepository.GetById(sales.ProductId);
            return _salesTransactonRepository.AddSales(new AddSalesTransactionDTO
            {
                ProductId = product.Id,
                Quantity = sales.Quantity,
                Rate = product.PerUnit

            });
        }

        public List<ViewSalesTransactionDTO> GetAll()
        {
            return _salesTransactonRepository.GetAllSales();
        }

        public ViewSalesTransactionDTO Update(UpdateSaleDTO sale)
        {
            var product = _productRepository.GetById(sale.ProductId);
            return _salesTransactonRepository.UpdateSales(new UpdateSalesTransactionDTO
            {
                ProductId = product.Id,
                Quantity = sale.Quantity,
                Rate = product.PerUnit,
                Id = sale.Id
            });
        }
    }
}
