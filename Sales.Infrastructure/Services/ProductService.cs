﻿using Sales.Application.DTOs.ProductDTOs;
using Sales.Application.IRepositories;
using Sales.Application.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infrastructure.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public ViewProductDTO Add(AddProductDTO newProduct)
        {
            return _productRepository.AddProduct(newProduct);
        }

        public ViewProductDTO Get(Guid Id)
        {
            return _productRepository.GetById(Id);
        }

        public List<ViewProductDTO> GetAll()
        {
            return _productRepository.AllProduct();
        }

        public ViewProductDTO Update(UpdateProductDTO product)
        {
            return _productRepository.UpdateProduct(product);
        }
    }
}
