﻿using Sales.Application.DTOs.InvoiceDTOs;
using Sales.Application.IRepositories;
using Sales.Application.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infrastructure.Services
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly ISalesTransactonRepository _salesTransactonRepository;

        public InvoiceService(IInvoiceRepository invoiceRepository, ISalesTransactonRepository salesTransactonRepository)
        {
            _invoiceRepository = invoiceRepository;
            _salesTransactonRepository = salesTransactonRepository;
        }

        public string GenrateInvoice(AddInvoiceDTO invoice)
        {
            try
            {
                decimal total = 0;
                foreach(var id in invoice.SaleIds)
                {
                    var sales = _salesTransactonRepository.GetById(id);
                    total += sales.Total;
                }
                _invoiceRepository.GenerateInvoice(new InsertInvoiceDTO
                {
                    CustomerId = invoice.CustomerId,
                    SaleIds = invoice.SaleIds,
                    Total = total,
                    GenerateInvoice = invoice.GenerateInvoice
                });
                return "invoice generated";
            }catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
