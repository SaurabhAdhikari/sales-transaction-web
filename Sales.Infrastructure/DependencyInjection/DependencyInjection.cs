﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sales.Application.IRepositories;
using Sales.Application.IServices;
using Sales.Infrastructure.Services;
using Sales.Infrastructure.Repositories;

namespace Sales.Infrastructure.DependencyInjection
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure (this IServiceCollection services)
        {
            #region repository injection
            services.AddTransient<IBaseRepository, BaseRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IAuthRepository, AuthRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ISalesTransactonRepository, SalesTransactionRepository>();
            services.AddTransient<IInvoiceRepository, InvoiceRepository>();
            

            #endregion

            #region service injection
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAuthService, AuthServices>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ISalesTransactionService, SalesTransactionService>();
            services.AddTransient<IInvoiceService, InvoiceService>();


            #endregion
            return services;
        }

    }
}
